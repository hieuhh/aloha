This project is a simple servlet that echos back the http request/response headers, body, parameters, etc.

To build the project, run 'ant all'.

To deploy, drop the dist/aloha.jar into your tomcat.

To test, hit http://localhost:8080/aloha

